# README #

### 13C-MFA ###

**Note**

* This is a Matlab based simulator that helps to analyze fluxes in isotopically steady state using a 13C-labeled carbon source.
* It was developed and tested in Windows 10 (64 bit), Matlab R2019b.

* By running the provided p-files using Matlab, flux analysis using 13C can be performed.
* For more detailed instructions, please refer to the provided ppt-file.
